package anime.applications.save;

public interface SavePhotoTaskCallback {
    void onSavePhotoTaskDone(String output, String fileString);
}