package anime.applications.save;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import anime.applications.CommonVariables;

/**
 * The save photo class allows for the user to save a high resolution version of
 * the puzzle they just solved.
 *
 * @author Rick
 */
public class SavePhotoTask extends AsyncTask<String, Void, String> {

    private CommonVariables commonVariables = CommonVariables.getInstance();

    private WeakReference<SavePhotoTaskCallback> mCallBack;

    private File file;

    public SavePhotoTask(SavePhotoTaskCallback callback) {
        this.mCallBack = new WeakReference<>(callback);
    }

    protected String doInBackground(String... urls) {

        boolean mExternalStorageAvailable;
        boolean mExternalStorageWriteable;

        // save current image to devices images folder
        String state = Environment.getExternalStorageState();
        // check if writing is an option
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other
            // states, but
            // all we need
            // to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }

        if (mExternalStorageAvailable && mExternalStorageWriteable) {
            // then write picture to phone
            File path = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

            int imageNumber;
            if (commonVariables.vertical) {
                imageNumber = commonVariables.serviceImageNumberPort;
            } else {
                imageNumber = commonVariables.serviceImageNumberLand;
            }

            final String name = commonVariables.artworkArrayList.get(imageNumber).getTitleOfArtwork() + ".jpg";
            final String urlName = commonVariables.artworkArrayList.get(imageNumber).getImageUrl();
            file = new File(path, name);

            // check for file in directory
            if (file.exists()) {
                return "Photo Exists Already!";
            } else {
                try {
                    boolean b1 = path.mkdirs();
                    boolean b2 = path.exists();
                    // Make sure the Pictures directory exists.
                    if (b1 || b2) {

                        // get file into input stream
                        URL url = new URL(urlName);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        Bitmap myBitmap = BitmapFactory.decodeStream(input);

                        boolean created = file.createNewFile();
                        if (created) {
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            myBitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
                            byte[] bitmapBytes = bos.toByteArray();

                            //write the bytes in file
                            FileOutputStream fos = new FileOutputStream(file);
                            fos.write(bitmapBytes);
                            fos.flush();
                            fos.close();

                            commonVariables.imagesSaved++;

                            return "Image Saved!";
                        }else{
                            return "File exists already.";
                        }
                    } else {
                        return "Could not make/access directory.";
                    }
                } catch (IOException e) {
                    return "Error making/accessing directory.";
                }
            }
        } else {
            return "Directory not available/writable.";
        }
    }

    @Override
    protected void onPostExecute(String message) {
        super.onPostExecute(message);

        final SavePhotoTaskCallback callBack = mCallBack.get();
        if (callBack != null) {
            if (file != null) {
                callBack.onSavePhotoTaskDone(message, file.toString());
            } else {
                callBack.onSavePhotoTaskDone(message, "");
            }
        }
    }
}
