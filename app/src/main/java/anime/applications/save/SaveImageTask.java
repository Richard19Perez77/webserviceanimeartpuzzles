package anime.applications.save;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import anime.applications.CommonVariables;

public class SaveImageTask extends AsyncTask<Void, Void, Void> {

    private CommonVariables common = CommonVariables.getInstance();

    protected Void doInBackground(Void... voids) {
        try {
            int imageNumber;
            if (common.vertical) {
                imageNumber = common.serviceImageNumberPort;
            } else {
                imageNumber = common.serviceImageNumberLand;
            }
            String fileName = common.artworkArrayList.get(imageNumber).getTitleOfArtwork() + ".jpg";
            Context context = common.errorTextView.getContext();
            File f = new File(context.getCacheDir(), fileName);
            boolean created = f.createNewFile();
            if (created) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                common.image.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                byte[] bitmapBytes = bos.toByteArray();

                //write the bytes in file
                FileOutputStream fos = new FileOutputStream(f);
                fos.write(bitmapBytes);
                fos.flush();
                fos.close();
            }
        } catch (IOException e) {
            // save error no sweat we can get it from the service next time
        } catch (NullPointerException ignored) {
            // context could be null
        }
        return null;
    }
}
