package anime.applications.save;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class SaveJsonTask extends AsyncTask<String, Void, Void> {

    private Context context;

    public SaveJsonTask(Context context) {
        this.context = context;
    }

    protected Void doInBackground(String... strings) {

        String result = null;
        if (!TextUtils.isEmpty(strings[0])) {
            result = strings[0];
        }

        String fileName = null;
        if (!TextUtils.isEmpty(strings[1])) {
            fileName = strings[1];
        }

        if (context != null && result != null && fileName != null) {
            try {
                FileOutputStream outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
                outputStream.write(result.getBytes());
                outputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
