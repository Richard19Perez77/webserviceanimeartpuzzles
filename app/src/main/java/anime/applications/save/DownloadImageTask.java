package anime.applications.save;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import anime.applications.puzzle.PuzzleSurface;

public class DownloadImageTask extends AsyncTask<String, Integer, Boolean> {

    private WeakReference<PuzzleSurface> mPuzzleSurface;
    private WeakReference<TextView> mPercentageTextView;
    private WeakReference<ProgressBar> mProgressBar;

    private Bitmap myBitmap;

    private int width, height;

    public DownloadImageTask(PuzzleSurface puzzleSurface, TextView percentageTextView, ProgressBar progressBar, int width, int height) {
        mPuzzleSurface = new WeakReference<>(puzzleSurface);
        mPercentageTextView = new WeakReference<>(percentageTextView);
        mProgressBar = new WeakReference<>(progressBar);
        this.width = width;
        this.height = height;

    }

    protected Boolean doInBackground(String... urls) {

        boolean imageLoaded = false;
        try {
            String fileName = urls[0];
            //check for filename to exists in local system
            File dir1 = mPuzzleSurface.get().getContext().getCacheDir();

            // list the folder under the directory
            File[] fileArray = dir1.listFiles();
            for (File file : fileArray) {
                String str = file.getName().toString();

                if (str.equals(fileName)) {

                    // load file into bitmap
                    try {

                        File imgFile = new File(file.getAbsolutePath());
                        if (imgFile.exists()) {
                            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                            if (myBitmap != null) {
                                myBitmap = Bitmap.createScaledBitmap(myBitmap,
                                       width, height, true);
                                imageLoaded = true;
                            }
                        }

                    } catch (Exception e) {
                        // and error here means we can try the service as well
                    }
                }
            }

            if (!imageLoaded) {

                URL url = new URL(urls[1]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int fileLength = connection.getContentLength();

                byte data[] = new byte[1024];
                long total = 0;
                int count;
                InputStream input = new BufferedInputStream(url.openStream());
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // Publish the progress
                    int progress = ((int) (total * 100 / fileLength));
                    publishProgress(progress);
                    output.write(data, 0, count);
                }
                byte[] bitmapData = output.toByteArray();
                myBitmap = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
                myBitmap = Bitmap.createScaledBitmap(myBitmap,
                        width, height, true);
            }
        } catch (IOException e) {
            return false;
        }

        return true;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if(mPercentageTextView.get() != null) {
            String res = values[0] + "%";
            mPercentageTextView.get().setText(res);
        }
    }

    @Override
    protected void onPostExecute(Boolean imageReceived) {
        super.onPostExecute(imageReceived);
        if (imageReceived) {

            final ProgressBar progressBar = mProgressBar.get();
            if (progressBar!= null) {
                progressBar.setVisibility(View.INVISIBLE);
            }

            final TextView percentageTextView = mPercentageTextView.get();
            if (percentageTextView!= null) {
                percentageTextView.setVisibility(View.INVISIBLE);
            }

            final PuzzleSurface puzzleSurface = mPuzzleSurface.get();
            if (puzzleSurface != null) {
                puzzleSurface.resumeOrCreatePuzzle(myBitmap);
            }
        } else {
            final PuzzleSurface puzzleSurface = mPuzzleSurface.get();
            if (puzzleSurface != null) {
                puzzleSurface.errorGettingImage();
            }
        }
    }
}
