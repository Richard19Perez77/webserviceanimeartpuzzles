
package anime.applications.service;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Artworks implements Parcelable{

    public static String TAG = "anime.applications.service.Artworks";

    @SerializedName("Artworks")
    @Expose
    private List<Artwork> artworks;

    protected Artworks(Parcel in) {
        artworks = in.createTypedArrayList(Artwork.CREATOR);
    }

    public static final Creator<Artworks> CREATOR = new Creator<Artworks>() {
        @Override
        public Artworks createFromParcel(Parcel in) {
            return new Artworks(in);
        }

        @Override
        public Artworks[] newArray(int size) {
            return new Artworks[size];
        }
    };

    public List<Artwork> getArtworks() {
        return artworks;
    }

    public void setArtworks(List<Artwork> artworks) {
        this.artworks = artworks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(artworks);
    }
}
