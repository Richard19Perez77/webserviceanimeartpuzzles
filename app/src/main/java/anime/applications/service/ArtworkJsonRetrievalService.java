package anime.applications.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;

import anime.applications.R;

/**
 * A class to get the JSON file with artwork information.
 */
public class ArtworkJsonRetrievalService extends IntentService {

    public static final String ARTWORK_SERVICE = "ARTWORK_SERVICE";
    public static final String ARTWORK_SERVICE_ERROR = "ARTWORK_SERVICE_ERROR";
    public static final String JSON_SAVE_LOCAL = "JSON_SAVE_LOCAL";
    public static final String RESULT = "RESULT";
    public static final String FILE_NAME = "FILE_NAME";

    String fileName = "";

    public ArtworkJsonRetrievalService() {
        super("ArtworkJsonRetrievalService");
    }

    @Override
    public void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String jsonString = getJsonFileString();
            try {
                if (jsonString != null && !jsonString.equals("")) {

                    // convert string to artworks
                    JSONObject obj = new JSONObject(jsonString);
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.registerTypeAdapter(Artwork.class, new JsonDeserializer<Artwork>() {

                        @Override
                        public Artwork deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            JsonObject jobject = json.getAsJsonObject();
                            try {
                                String titleOfArtwork = jobject.get("titleOfArtwork").getAsString();
                                if (titleOfArtwork.equals("")) return null;
                                String titleOfArtist = jobject.get("titleOfArtist").getAsString();
                                if (titleOfArtist.equals("")) return null;
                                String urlOfArtist = jobject.get("urlOfArtist").getAsString();
                                if (urlOfArtist.equals("")) return null;
                                String imageUrl = jobject.get("imageUrl").getAsString();
                                if (imageUrl.equals("")) return null;

                                return new Artwork(titleOfArtwork, titleOfArtist, urlOfArtist, imageUrl);
                            } catch (NullPointerException npe) {
                                return null;
                            }
                        }
                    });
                    Gson gson = gsonBuilder.create();
                    final Artworks artworks = gson.fromJson(obj.toString(), Artworks.class);

                    // remove nulls from list
                    if (artworks.getArtworks() != null) {
                        artworks.getArtworks().removeAll(Collections.singleton(null));
                    }

                    if (artworks.getArtworks() == null || artworks.getArtworks().isEmpty()) {
                        errorHandler();
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(ARTWORK_SERVICE);
                                intent.putExtra(Artworks.TAG, artworks);
                                sendBroadcast(intent);
                            }
                        });
                    }
                } else {
                    errorHandler();
                }
            } catch (JSONException e) {
                errorHandler();
            }
        }
    }

    private void saveLocalJSONHandler(final String result) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(JSON_SAVE_LOCAL);
                intent.putExtra(RESULT, result);
                intent.putExtra(FILE_NAME, fileName);
                sendBroadcast(intent);
            }
        });
    }

    private void errorHandler() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ARTWORK_SERVICE_ERROR);
                sendBroadcast(intent);
            }
        });
    }

    /**
     * Use the internet to get the json file or if on fail check for a backup
     *
     * @return the string that will be parsed into our artwork list
     */
    public String getJsonFileString() {

        // hit the server first
        HttpURLConnection urlConnection = null;
        String result;
        try {

            Resources res = getResources();
            boolean vertical = res.getBoolean(R.bool.vertical);
            if (vertical) {
                fileName = "artworklist_port.json";
            } else {
                fileName = "artworklist_land.json";
            }

            String jsonUrl = "https://www.mobileapplications009.com/puzzles/android/" + fileName;
            URL url = new URL(jsonUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            result = sb.toString();

            saveLocalJSONHandler(result);
        } catch (MalformedURLException e) {

            // on bad url check for backup
            result = loadLocalJson();
            if (result.equals("")) {
                errorHandler();
            }
        } catch (IOException e) {

            // something went wrong, check for backup
            result = loadLocalJson();
            if (result.equals("")) {
                errorHandler();
            }
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return result;
    }

    private String loadLocalJson() {
        File directory = getBaseContext().getFilesDir();
        File file = new File(directory, fileName);
        String result = "";
        if (file.exists()) {
            int length = (int) file.length();
            byte[] bytes = new byte[length];
            FileInputStream in = null;
            try {
                in = new FileInputStream(file);
                in.read(bytes);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            result = new String(bytes);
        }
        return result;
    }
}
