
package anime.applications.service;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Artwork implements Parcelable {

    @SerializedName("titleOfArtwork")
    @Expose
    @NonNull
    private String titleOfArtwork;
    @SerializedName("titleOfArtist")
    @Expose
    @NonNull
    private String titleOfArtist;
    @SerializedName("urlOfArtist")
    @Expose
    @NonNull
    private String urlOfArtist;
    @SerializedName("imageUrl")
    @Expose
    @NonNull
    private String imageUrl;

    protected Artwork(Parcel in) {
        titleOfArtwork = in.readString();
        titleOfArtist = in.readString();
        urlOfArtist = in.readString();
        imageUrl = in.readString();
    }

    public Artwork(String titleOfArtwork, String titleOfArtist, String urlOfArtist, String imageUrl) {
        this.titleOfArtist = titleOfArtist;
        this.titleOfArtwork = titleOfArtwork;
        this.urlOfArtist = urlOfArtist;
        this.imageUrl = imageUrl;
    }

    public static final Creator<Artwork> CREATOR = new Creator<Artwork>() {
        @Override
        public Artwork createFromParcel(Parcel in) {
            return new Artwork(in);
        }

        @Override
        public Artwork[] newArray(int size) {
            return new Artwork[size];
        }
    };

    public String getTitleOfArtwork() {
        return titleOfArtwork;
    }

    public void setTitleOfArtwork(String titleOfArtwork) {
        this.titleOfArtwork = titleOfArtwork;
    }

    public String getTitleOfArtist() {
        return titleOfArtist;
    }

    public void setTitleOfArtist(String titleOfArtist) {
        this.titleOfArtist = titleOfArtist;
    }

    public String getUrlOfArtist() {
        return urlOfArtist;
    }

    public void setUrlOfArtist(String urlOfArtist) {
        this.urlOfArtist = urlOfArtist;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(titleOfArtwork);
        dest.writeString(titleOfArtist);
        dest.writeString(urlOfArtist);
        dest.writeString(imageUrl);
    }
}
