package anime.applications.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import anime.applications.CommonVariables;
import anime.applications.MyMediaPlayer;
import anime.applications.MySoundPool;
import anime.applications.R;
import anime.applications.puzzle.PuzzleSurface;
import anime.applications.service.ArtworkJsonRetrievalService;

/**
 * Fragment class to hold the puzzle being played.
 */
public class OnlinePuzzleFragment extends Fragment {

    public static final String TAG = "anime.applications.fragments.OnlinePuzzleFragment";

    public PuzzleSurface puzzleSurface;
    public MyMediaPlayer myMediaPlayer;
    public MySoundPool mySoundPool;
    public SharedPreferences sharedpreferences;
    public CommonVariables common = CommonVariables.getInstance();
    public NoisyAudioStreamReceiver noisyAudioStreamReceiver;

    private static final String PUZZLE_LOG = "puzzleLog";

    /**
     * Start of receiver inner class to handle headphones becoming unplugged
     */
    public class NoisyAudioStreamReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (common.isLogging)
                Log.d(PUZZLE_LOG, "onReceive NoisyAudioStreamReceiver");

            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent
                    .getAction())) {
                // quiet the media player
                if (myMediaPlayer != null) {
                    myMediaPlayer.setNewVolume(0.1f);
                }
            }
        }
    }

    private IntentFilter intentFilter = new IntentFilter(
            AudioManager.ACTION_AUDIO_BECOMING_NOISY);

    private void startPlayback() {
        if (common.isLogging)
            Log.d(PUZZLE_LOG, "startPlayback NoisyAudioStreamReceiver");
        getActivity().registerReceiver(noisyAudioStreamReceiver, intentFilter);
    }

    private void stopPlayback() {
        if (common.isLogging)
            Log.d(PUZZLE_LOG, "stopPlayback NoisyAudioStreamReceiver");

        getActivity().unregisterReceiver(noisyAudioStreamReceiver);
    }

    /**
     * @param savedInstanceState The fragment saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.puzzle_layout, container,
                false);
        referenceUIComponents(view);
        audioInit();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        Resources res = getResources();
        common.vertical = res.getBoolean(R.bool.vertical);

        getSharedPrefs();

        common.isImageLoaded = false;

        common.percentageTextView.bringToFront();
        common.progressBar.bringToFront();

        Intent intent = new Intent(getContext(), ArtworkJsonRetrievalService.class);
        getContext().startService(intent);
    }

    /**
     * Get volume for application sound and check if music should be from app or device.
     */
    @Override
    public void onResume() {
        super.onResume();
        AudioManager audioManager = (AudioManager) getActivity()
                .getSystemService(Context.AUDIO_SERVICE);

        float streamVolume = (float) audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);

        common.volume = streamVolume
                / (float) audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        noisyAudioStreamReceiver = new NoisyAudioStreamReceiver();
        startPlayback();

        if (common.playMusic) {
            myMediaPlayer.resume();
        } else {
            myMediaPlayer.abandonFocus();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_puzzle, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_puzzle:
                if (common.isImageLoaded) {
                    puzzleSurface.menuNewPuzzleSelected();
                }
                return true;
            case R.id.music_toggle:
                puzzleSurface.toggleMusic();
                return true;
            case R.id.set_toggle:
                puzzleSurface.toggleSetSound();
                return true;
            case R.id.win_toggle:
                puzzleSurface.toggleWinSound();
                return true;
            case R.id.border_toggle:
                puzzleSurface.toggleBorder();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Get the previous puzzle and stats.
     */
    private void getSharedPrefs() {
        if (common.isLogging)
            Log.d(PUZZLE_LOG, "getSharedPrefs OnlinePuzzleFragment");

        sharedpreferences = getActivity().getSharedPreferences(
                getString(R.string.MY_PREFERENCES), Context.MODE_PRIVATE);

        // check for all to be loaded here
        boolean isValid;

        int posImagePort = 0;
        if (sharedpreferences.contains(getString(R.string.SERVICEIMAGENUMBERPORT))) {
            posImagePort = sharedpreferences.getInt(
                    getString(R.string.SERVICEIMAGENUMBERPORT), 0);
            isValid = true;
        } else {
            isValid = false;
        }

        int posImageLand = 0;
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.SERVICEIMAGENUMBERLAND))) {
                posImageLand = sharedpreferences.getInt(
                        getString(R.string.SERVICEIMAGENUMBERLAND), 0);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        int prefSides = 3;
        if (isValid) {
            if (common.vertical) {
                if (sharedpreferences.contains(getString(R.string.SIDES))) {
                    prefSides = sharedpreferences.getInt(
                            getString(R.string.SIDES), 3);
                    isValid = true;
                } else {
                    isValid = false;
                }
            } else {
                if (sharedpreferences.contains(getString(R.string.SIDES_LAND))) {
                    prefSides = sharedpreferences.getInt(
                            getString(R.string.SIDES_LAND), 3);
                    isValid = true;
                } else {
                    isValid = false;
                }
            }
        }

        Long currentTime = 0L;
        if (isValid) {
            if (common.vertical) {
                if (sharedpreferences.contains(getString(R.string.TIME))) {
                    currentTime = sharedpreferences.getLong(getString(R.string.TIME), 0L);
                    isValid = true;
                } else {
                    isValid = false;
                }
            } else {
                if (sharedpreferences.contains(getString(R.string.TIME_LAND))) {
                    currentTime = sharedpreferences.getLong(getString(R.string.TIME_LAND), 0L);
                    isValid = true;
                } else {
                    isValid = false;
                }
            }
        }

        // only continue if there is an image from the previous puzzle
        String slots = "";
        if (isValid) {
            if (common.vertical) {
                if (sharedpreferences.contains(getString(R.string.SLOTS))) {
                    slots = sharedpreferences.getString(getString(R.string.SLOTS), "");
                    if (slots.equals("") || slots.length() < 2) {
                        isValid = false;
                    } else {
                        String[] slotArr = slots.split(",");
                        int expectedTotal = sumToPositiveN(slotArr.length - 1);
                        int actualTotal = 0;
                        for (String aSlotArr : slotArr) {
                            try {
                                int temp = Integer.parseInt(aSlotArr);
                                actualTotal += temp;
                            } catch (NumberFormatException nfe) {
                                isValid = false;
                            }
                        }

                        if (expectedTotal != actualTotal) {
                            isValid = false;
                        }
                    }
                }
            } else {
                if (sharedpreferences.contains(getString(R.string.SLOTS_LAND))) {
                    slots = sharedpreferences.getString(getString(R.string.SLOTS_LAND), "");
                    if (slots.equals("") || slots.length() < 2) {
                        isValid = false;
                    } else {
                        String[] slotArr = slots.split(",");
                        int expectedTotal = sumToPositiveN(slotArr.length - 1);
                        int actualTotal = 0;
                        for (String aSlotArr : slotArr) {
                            try {
                                int temp = Integer.parseInt(aSlotArr);
                                actualTotal += temp;
                            } catch (NumberFormatException nfe) {
                                isValid = false;
                            }
                        }

                        if (expectedTotal != actualTotal) {
                            isValid = false;
                        }
                    }
                }
            }
        }

        boolean playTap = false;
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.SOUND))) {
                playTap = sharedpreferences.getBoolean(getString(R.string.SOUND), true);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        boolean playChime = false;
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.CHIME))) {
                playChime = sharedpreferences.getBoolean(getString(R.string.CHIME),
                        true);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        boolean playMusic = false;
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.MUSIC))) {
                playMusic = sharedpreferences.getBoolean(getString(R.string.MUSIC),
                        true);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        boolean drawBorders = false;
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.BORDER))) {
                drawBorders = sharedpreferences.getBoolean(getString(R.string.BORDER),
                        true);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        int posSound = 0;
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.POSITION))) {
                posSound = sharedpreferences.getInt(getString(R.string.POSITION), 0);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        if (isValid) {
            common.serviceImageNumberPort = posImagePort;
            common.serviceImageNumberLand = posImageLand;
            common.currentSoundPosition = posSound;
            common.sides = prefSides;
            common.drawBorders = drawBorders;
            common.playMusic = playMusic;
            common.playChimeSound = playChime;
            common.playTapSound = playTap;
            common.setSlots(slots);
            common.currPuzzleTime = currentTime;
            common.resumePreviousPuzzle = true;
        } else {
            common.resumePreviousPuzzle = false;
        }

        //start of saved stats
        if (sharedpreferences.contains(getString(R.string.PUZZLES_SOLVED))) {
            common.puzzlesSolved = sharedpreferences.getInt(getString(R.string.PUZZLES_SOLVED), 0);
        }

        if (sharedpreferences.contains(getString(R.string.TWO_SOLVE_COUNT))) {
            common.fourPiecePuzzleSolvedCount = sharedpreferences.getInt(getString(R.string.TWO_SOLVE_COUNT), 0);
        }

        if (sharedpreferences.contains(getString(R.string.TWO_SOLVE_TIME))) {
            common.fourRecordSolveTime = sharedpreferences.getLong(getString(R.string.TWO_SOLVE_TIME), 0);
        }

        if (sharedpreferences.contains(getString(R.string.THREE_SOLVE_COUNT))) {
            common.ninePiecePuzzleSolvedCount = sharedpreferences.getInt(getString(R.string.THREE_SOLVE_COUNT), 0);
        }

        if (sharedpreferences.contains(getString(R.string.THREE_SOLVE_TIME))) {
            common.nineRecordSolveTime = sharedpreferences.getLong(getString(R.string.THREE_SOLVE_TIME), 0);
        }

        if (sharedpreferences.contains(getString(R.string.FOUR_SOLVE_COUNT))) {
            common.sixteenPiecePuzzleSolvedCount = sharedpreferences.getInt(getString(R.string.FOUR_SOLVE_COUNT), 0);
        }

        if (sharedpreferences.contains(getString(R.string.FOUR_SOLVE_TIME))) {
            common.sixteenRecordSolveTime = sharedpreferences.getLong(getString(R.string.FOUR_SOLVE_TIME), 0);
        }

        if (sharedpreferences.contains(getString(R.string.FIVE_SOLVE_COUNT))) {
            common.twentyfivePiecePuzzleSolvedCount = sharedpreferences.getInt(getString(R.string.FIVE_SOLVE_COUNT), 0);
        }

        if (sharedpreferences.contains(getString(R.string.FIVE_SOLVE_TIME))) {
            common.twentyfiveRecordSolveTime = sharedpreferences.getLong(getString(R.string.FIVE_SOLVE_TIME), 0);
        }

        if (sharedpreferences.contains(getString(R.string.SIX_SOLVE_COUNT))) {
            common.thirtysixPiecePuzzleSolvedCount = sharedpreferences.getInt(getString(R.string.SIX_SOLVE_COUNT), 0);
        }

        if (sharedpreferences.contains(getString(R.string.SIX_SOLVE_TIME))) {
            common.thirtysixRecordsSolveTime = sharedpreferences.getLong(getString(R.string.SIX_SOLVE_TIME), 0);
        }

        if (sharedpreferences.contains(getString(R.string.SEVEN_SOLVE_COUNT))) {
            common.fourtyninePiecePuzzleSolvedCount = sharedpreferences.getInt(getString(R.string.SEVEN_SOLVE_COUNT), 0);
        }

        if (sharedpreferences.contains(getString(R.string.SEVEN_SOLVE_TIME))) {
            common.fourtynineRecordsSolveTime = sharedpreferences.getLong(getString(R.string.SEVEN_SOLVE_TIME), 0);
        }

        if (sharedpreferences.contains(getString(R.string.IMAGES_SAVED))) {
            common.imagesSaved = sharedpreferences.getInt(getString(R.string.IMAGES_SAVED), 0);
        }

        if (sharedpreferences.contains(getString(R.string.BLOG_LINKS_TRAVERSED))) {
            common.blogLinksTraversed = sharedpreferences.getInt(getString(R.string.BLOG_LINKS_TRAVERSED), 0);
        }

        if (sharedpreferences.contains(getString(R.string.MUSIC_SAVED))) {
            common.musicSaved = sharedpreferences.getInt(getString(R.string.MUSIC_SAVED), 0);
        }
    }

    /**
     * Helper method to get total of all ints from 0 to n;
     *
     * @param n the number to sum accumulate to
     * @return the total
     */
    private int sumToPositiveN(int n) {
        if (n <= 0)
            return 0;
        return sumToPositiveN(n - 1) + n;
    }

    /**
     * Set up media player and sound pool.
     */
    private void audioInit() {
        // create new my media player
        myMediaPlayer = new MyMediaPlayer(getContext());
        puzzleSurface.myMediaPlayer = myMediaPlayer;

        mySoundPool = new MySoundPool(15, AudioManager.STREAM_MUSIC, 100);
        mySoundPool.init(getContext());
        common.mySoundPool = mySoundPool;
    }

    /**
     * Reference our view objects and create listeners for them.
     *
     * @param view get all our views from this root view
     */
    private void referenceUIComponents(View view) {
        puzzleSurface = view.findViewById(R.id.puzzle);

        common.progressBar = view.findViewById(R.id.progressBar);

        common.percentageTextView = view.findViewById(R.id.percentTextView);

        common.errorTextView = view.findViewById(R.id.errorTextView);

        common.mNextButton = view.findViewById(R.id.nextButton);
        common.mNextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                common.hideButtons();
                common.isImageLoaded = false;

                if (common.progressBar != null && common.progressBar.getVisibility() == View.INVISIBLE) {
                    common.progressBar.setVisibility(View.VISIBLE);
                    common.progressBar.bringToFront();
                }

                if (common.percentageTextView != null && common.percentageTextView.getVisibility() == View.INVISIBLE) {
                    common.percentageTextView.setVisibility(View.VISIBLE);
                    common.percentageTextView.setText("Starting Download");
                    common.percentageTextView.bringToFront();
                }

                if (common.vertical) {
                    common.serviceImageNumberPort++;
                    if (common.serviceImageNumberPort >= common.artworkArrayList.size()) {
                        common.serviceImageNumberPort = 0;
                    }
                } else {
                    common.serviceImageNumberLand++;
                    if (common.serviceImageNumberLand >= common.artworkArrayList.size()) {
                        common.serviceImageNumberLand = 0;
                    }
                }

                puzzleSurface.loadImage();
            }
        });

        common.devartButton = view
                .findViewById(R.id.devartButton);

        common.devartButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                puzzleSurface.devartActivity();
            }
        });

        common.wordpressLinkButton = view
                .findViewById(R.id.wordpressButton);
        common.wordpressLinkButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                puzzleSurface.wordpressActivity();
            }
        });

        common.saveMusicButton = view.findViewById(R.id.saveMusicButton);
        common.saveMusicButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                puzzleSurface.saveMusic();
            }
        });

        common.saveImageButton = view.findViewById(R.id.saveImageButton);
        common.saveImageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                puzzleSurface.saveImage();
            }
        });

        common.textViewSolve = view.findViewById(R.id.scoreText);
    }

    public void loadImage() {
        common.hideButtons();
        common.isImageLoaded = false;
        puzzleSurface.loadImage();
    }

    public void errorState() {
        if (common.progressBar != null && common.progressBar.isShown()) {
            common.progressBar.setVisibility(View.INVISIBLE);
        }

        if (common.percentageTextView != null && common.percentageTextView.isShown()) {
            common.percentageTextView.setVisibility(View.INVISIBLE);
        }

        if (common.errorTextView != null && !common.errorTextView.isShown()) {
            common.errorTextView.setVisibility(View.VISIBLE);
            common.errorTextView.bringToFront();
        }
    }

    public void reSaveImage() {
        puzzleSurface.saveImage();
    }

    public void reSaveMusic() {
        puzzleSurface.saveMusic();
    }

    @Override
    public void onPause() {
        super.onPause();

        stopPlayback();

        if (myMediaPlayer != null) {
            myMediaPlayer.pause();
        }

        if (puzzleSurface != null) {
            puzzleSurface.onPause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        myMediaPlayer.onStop();

        String slotString = puzzleSurface.getSlotString();
        Long dateLong = common.currPuzzleTime;

        Editor editor = sharedpreferences.edit();

        editor.putInt(getString(R.string.SERVICEIMAGENUMBERPORT), common.serviceImageNumberPort);
        editor.putInt(getString(R.string.SERVICEIMAGENUMBERLAND), common.serviceImageNumberLand);

        if (common.vertical) {
            editor.putInt(getString(R.string.SIDES), common.sides);
            editor.putString(getString(R.string.SLOTS), slotString);
            editor.putLong(getString(R.string.TIME), dateLong);
        } else {
            editor.putInt(getString(R.string.SIDES_LAND), common.sides);
            editor.putString(getString(R.string.SLOTS_LAND), slotString);
            editor.putLong(getString(R.string.TIME_LAND), dateLong);
        }

        editor.putBoolean(getString(R.string.SOUND), common.playTapSound);
        editor.putBoolean(getString(R.string.MUSIC), common.playMusic);
        editor.putBoolean(getString(R.string.CHIME), common.playChimeSound);
        editor.putBoolean(getString(R.string.BORDER), common.drawBorders);
        editor.putInt(getString(R.string.POSITION), common.currentSoundPosition);
        editor.putInt(getString(R.string.MUSIC_SAVED), common.musicSaved);
        editor.putInt(getString(R.string.PUZZLES_SOLVED), common.puzzlesSolved);
        editor.putInt(getString(R.string.IMAGES_SAVED), common.imagesSaved);
        editor.putInt(getString(R.string.BLOG_LINKS_TRAVERSED), common.blogLinksTraversed);
        editor.putInt(getString(R.string.TWO_SOLVE_COUNT), common.fourPiecePuzzleSolvedCount);
        editor.putLong(getString(R.string.TWO_SOLVE_TIME), common.fourRecordSolveTime);
        editor.putInt(getString(R.string.THREE_SOLVE_COUNT), common.ninePiecePuzzleSolvedCount);
        editor.putLong(getString(R.string.THREE_SOLVE_TIME), common.nineRecordSolveTime);
        editor.putInt(getString(R.string.FOUR_SOLVE_COUNT), common.sixteenPiecePuzzleSolvedCount);
        editor.putLong(getString(R.string.FOUR_SOLVE_TIME), common.sixteenRecordSolveTime);
        editor.putInt(getString(R.string.FIVE_SOLVE_COUNT), common.twentyfivePiecePuzzleSolvedCount);
        editor.putLong(getString(R.string.FIVE_SOLVE_TIME), common.twentyfiveRecordSolveTime);
        editor.putInt(getString(R.string.SIX_SOLVE_COUNT), common.thirtysixPiecePuzzleSolvedCount);
        editor.putLong(getString(R.string.SIX_SOLVE_TIME), common.thirtysixRecordsSolveTime);
        editor.putInt(getString(R.string.SEVEN_SOLVE_COUNT), common.fourtyninePiecePuzzleSolvedCount);
        editor.putLong(getString(R.string.SEVEN_SOLVE_TIME), common.fourtynineRecordsSolveTime);
        editor.apply();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mySoundPool != null) {
            mySoundPool.release();
            mySoundPool = null;
        }

        if (myMediaPlayer != null) {
            myMediaPlayer.cleanUp();
            myMediaPlayer = null;
        }

        if (puzzleSurface != null) {
            puzzleSurface.cleanUp();
            puzzleSurface = null;
        }
    }
}