package anime.applications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import anime.applications.fragments.OnlinePuzzleFragment;
import anime.applications.fragments.PuzzleStatsFragment;
import anime.applications.save.SaveJsonTask;
import anime.applications.save.SaveMusicService;
import anime.applications.save.SavePhotoTaskCallback;
import anime.applications.service.ArtworkJsonRetrievalService;
import anime.applications.service.Artworks;

/**
 * The main activity of the application, sets the fragment placeholder to the container view.
 * On resume it will start and animation for the user.
 * Test new images by changing value in AdjustablePuzzle Class line 99.
 */
public class MainActivity extends AppCompatActivity implements SavePhotoTaskCallback {

    CoordinatorLayout coordinatorLayout;

    Toast toast;

    public static final int WRITE_EXTERNAL_STORAGE_IMAGE = 1;
    public static final int WRITE_EXTERNAL_STORAGE_SAVE = 2;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case ArtworkJsonRetrievalService.ARTWORK_SERVICE: {
                        // we have art urls for downloading images
                        Artworks example = intent.getParcelableExtra(Artworks.TAG);
                        CommonVariables.getInstance().artworkArrayList = new ArrayList<>(example.getArtworks());
                        updateOnlinePuzzleFragmentToLoadImage();
                        break;
                    }
                    case ArtworkJsonRetrievalService.ARTWORK_SERVICE_ERROR: {
                        // error in downloading use local images fragment
                        updateOnlinePuzzleFragmentToErrorState();
                        break;
                    }
                    case SaveMusicService.MUSIC_SAVED: {
                        CommonVariables.getInstance().musicSaved++;
                        updatePuzzleStats();
                        break;
                    }
                    case ArtworkJsonRetrievalService.JSON_SAVE_LOCAL: {
                        String result = "";
                        if (intent.hasExtra(ArtworkJsonRetrievalService.RESULT)) {
                            result = intent.getStringExtra("result");
                        }

                        String fileName = "";
                        if (intent.hasExtra(ArtworkJsonRetrievalService.FILE_NAME)) {
                            fileName = intent.getStringExtra(ArtworkJsonRetrievalService.FILE_NAME);
                        }

                        new SaveJsonTask(getApplicationContext()).execute(result, fileName);
                    }
                    default:
                        break;
                }
            }
        }
    };

    /**
     * Start the create method by checking for the current google play services library. Attach the placeholder fragment if not already saved in previous state.
     *
     * @param savedInstanceState the saved instance of the activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);

        coordinatorLayout = findViewById(R.id.fragment_container);
        CommonVariables.getInstance().coordinatorLayout = coordinatorLayout;
    }

    /**
     * Start logging for Facebook user's. Begin the screen animation for starting the puzzle game.
     */
    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(receiver, new IntentFilter(
                SaveMusicService.MUSIC_SAVED));

        registerReceiver(receiver, new IntentFilter(
                ArtworkJsonRetrievalService.ARTWORK_SERVICE));

        registerReceiver(receiver, new IntentFilter(
                ArtworkJsonRetrievalService.ARTWORK_SERVICE_ERROR));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_IMAGE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    OnlinePuzzleFragment onlinePuzzleFragment =
                            (OnlinePuzzleFragment) getSupportFragmentManager().findFragmentByTag(OnlinePuzzleFragment.TAG);
                    if (onlinePuzzleFragment != null) {
                        onlinePuzzleFragment.reSaveImage();
                    }
                } else {
                    Snackbar.make(coordinatorLayout, R.string.permission_not_granted, Snackbar.LENGTH_SHORT).show();
                }
                return;
            }
            case WRITE_EXTERNAL_STORAGE_SAVE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    OnlinePuzzleFragment onlinePuzzleFragment =
                            (OnlinePuzzleFragment) getSupportFragmentManager().findFragmentByTag(OnlinePuzzleFragment.TAG);
                    if (onlinePuzzleFragment != null) {
                        onlinePuzzleFragment.reSaveMusic();
                    }
                } else {
                    Snackbar.make(coordinatorLayout, R.string.permission_not_granted, Snackbar.LENGTH_SHORT).show();
                }
            }
        }
    }


    /**
     * Stop facebook tracking events.
     */
    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(receiver);
    }

    /**
     * Create the default main menu layout.
     *
     * @param menu the visible menu for the user
     * @return true after inflating
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * The main menu option is to show the stats fragment if not already, depending on layout, this might not be an option.
     *
     * @param item item selected from menu
     * @return true if hanlded or pass to base activity
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // use a switch to run the handler for each item selected
        switch (item.getItemId()) {
            case R.id.menu_stats:
                switchToStatsFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Helper method to show toast messages to the user.
     *
     * @param message the message to show
     */
    public void showToast(String message) {
        if (toast == null) {
            toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
        }
        if (toast.getView().isShown()) {
            toast.cancel();
        }
        toast.setText(message);
        toast.show();
    }

    /**
     * Fragment switching is setting the stats fragment on top of the puzzle, this way when the user backs out of the stats the puzzle is still playable.
     */
    private void switchToStatsFragment() {
        PuzzleStatsFragment fragment = (PuzzleStatsFragment) getSupportFragmentManager().findFragmentByTag(PuzzleStatsFragment.TAG);
        if (fragment == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, PuzzleStatsFragment.newInstance(), PuzzleStatsFragment.TAG).addToBackStack("puzzle").commit();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onSavePhotoTaskDone(String message, String fileString) {
        if (!fileString.equals("")) {
            MediaScannerConnection
                    .scanFile(
                            this,
                            new String[]{fileString},
                            null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(
                                        String path, Uri uri) {
                                    // new image is in phone
                                    // database now for use

                                }
                            });
        }

        showToast(message);
        updatePuzzleStats();
    }

    public void updatePuzzleStats() {
        PuzzleStatsFragment fragment = (PuzzleStatsFragment) getSupportFragmentManager().findFragmentByTag(PuzzleStatsFragment.TAG);
        if (fragment != null) {
            fragment.updatePuzzleStats();
        }
    }

    private void updateOnlinePuzzleFragmentToLoadImage() {
        OnlinePuzzleFragment fragment = (OnlinePuzzleFragment) getSupportFragmentManager().findFragmentByTag(OnlinePuzzleFragment.TAG);
        if (fragment != null) {
            fragment.loadImage();
        }
    }

    private void updateOnlinePuzzleFragmentToErrorState() {
        OnlinePuzzleFragment fragment = (OnlinePuzzleFragment) getSupportFragmentManager().findFragmentByTag(OnlinePuzzleFragment.TAG);
        if (fragment != null) {
            fragment.errorState();
        }
    }
}